FROM golang:alpine AS build-env
RUN mkdir -p $GOPATH/src/app
ADD . $GOPATH/src/app
WORKDIR $GOPATH/src/app
RUN go build -o ./golang-service

FROM alpine
WORKDIR /app
COPY --from=build-env /go/src/app/golang-service /app
COPY --from=build-env /go/src/app/templates/ /app/templates
COPY --from=build-env /go/src/app/static/ /app/static
ENV PORT=5000
EXPOSE 5000
ENTRYPOINT /app/golang-service
#CMD ["/golang-service"]
